import React from "react";
import Navbar from "../components/Navbar";


function Inicio() {
    return (
        <div className="content">
            <Navbar />
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-ms-9 col-md-8">
                        <div className="card card-body m-2">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi nemo amet, culpa iste animi cupiditate optio expedita quod exercitationem! Sint voluptas doloremque facere. Dicta, veniam hic dolor amet iste animi!
                                    <br/>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur mollitia magnam obcaecati labore, accusantium expedita odit ipsum adipisci eaque nulla nesciunt suscipit velit sequi, libero ut beatae ducimus dolores placeat.
                                </div>
                                <div className="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam accusamus facilis enim maiores labore repudiandae aut modi atque. Nemo dolor explicabo, illum? Laboriosam sit magni delectus perspiciatis quod omnis voluptate!</div>
                                <div className="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi vero tempore ut ipsa eius porro perspiciatis nam, ducimus aliquid quas obcaecati. Quo molestiae aperiam possimus id accusamus vitae adipisci hic?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos magnam rerum, modi iusto, sed in neque saepe ipsam! Consequuntur itaque fugiat quasi aperiam, odio totam ipsum deleniti ut necessitatibus in.</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-ms-3 col-md-4">
                        <div className="card card-body bg-info m-2">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime dolores magni, deserunt voluptas non a? Laboriosam ratione aspernatur at consectetur aliquam, dolore, quidem dolores beatae, magni corrupti, cumque! Asperiores, illum?
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates, voluptatum modi in libero! Ex suscipit beatae nisi quo vero molestias, magni obcaecati molestiae quisquam maiores itaque quis cum, rem deleniti?
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus labore hic aliquam nam nulla similique voluptatem, in totam repellendus magni et fugit, aut facilis, reprehenderit ipsa impedit dicta sequi quos.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Inicio;